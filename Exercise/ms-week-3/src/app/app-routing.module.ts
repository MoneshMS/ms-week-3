import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivateGuard } from './guards/activate.guard';
import { CanDeactivateGuard } from './guards/deactivate.guard';
import { CreateComponent } from './notification/create/create.component';
import { ListComponent } from './notification/list/list.component';
import { TimerComponent } from './timer/timer.component';

const routes: Routes = [
  {
    path: 'timer',
    component: TimerComponent,
    canActivate: [ActivateGuard],
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: 'notification',
    children: [
      { path: '', pathMatch: 'full', redirectTo: '/notification/list' },
      { path: 'list', component: ListComponent },
      { path: 'create', component: CreateComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
