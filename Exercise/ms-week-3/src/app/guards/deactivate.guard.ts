import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanDeactivate,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { TimerComponent } from '../timer/timer.component';

@Injectable({
  providedIn: 'root',
})
export class CanDeactivateGuard implements CanDeactivate<TimerComponent> {
  constructor(private authService: AuthService) {}

  canDeactivate(
    component: TimerComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
      const res = window.prompt('Are you sure want to leave? y: Yes; n: No');
      this.authService.updateUserStatus();
    if (res === 'y') {
      return true;
    }
    return false;
  }
}
