export interface Notification {
  message: string;
  type: 'error' | 'success' | 'warn' | 'info';
  title?: string;
  status: boolean;
}
