import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Notification } from 'src/app/models/notification';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  constructor(
    private notificationService: NotificationService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onFormSubmitted(form: NgForm) {
    const notification: Notification = {
      message: form.value.notificationMsg,
      type: form.value.type,
      status: false,
    };

    this.notificationService.AddNewNotificationMessage(notification);
    this.router.navigate(['notification/list']);
  }
}
