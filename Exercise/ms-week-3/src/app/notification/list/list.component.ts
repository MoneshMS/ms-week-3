import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Notification } from 'src/app/models/notification';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  notificationArray: Notification[] = [];

  constructor(private notificationService: NotificationService) {}

  ngOnInit(): void {
    this.notificationArray = this.notificationService.getNotificationMessages();
  }

  openNotification(i: number) {
    this.notificationService.openMatSnackBar(i);
    this.notificationArray = this.notificationService.getNotificationMessages();
  }
}
