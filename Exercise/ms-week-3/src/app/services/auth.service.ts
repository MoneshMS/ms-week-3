import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user = true;

  constructor() {}

  getUserStatus() {
    return this.user;
  }

  updateUserStatus() {
    this.user = !this.user;
  }
}
