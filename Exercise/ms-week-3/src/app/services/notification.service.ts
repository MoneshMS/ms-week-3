import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Notification } from '../models/notification';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  NotificationMessages: Notification[] = [];

  notificationColors = {
    success: ['success'],
    error: ['error'],
    warn: ['warn'],
    info: ['info'],
  };

  constructor(private snackbar: MatSnackBar) {}

  openMatSnackBar(id: number) {
    const notification = this.NotificationMessages[id];
    notification.status = true;
    this.snackbar.open(notification.message, 'x', {
      duration: 2000,
      panelClass: this.notificationColors[notification.type],
      verticalPosition: 'top',
    });
  }

  getNotificationMessages() {
    return this.NotificationMessages;
  }

  AddNewNotificationMessage(notification: Notification) {
    this.NotificationMessages.push(notification);
  }
}
