import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent implements OnInit {
  @Input() timer!: number;
  data: number = 0;

  constructor() {}

  ngOnInit(): void {
    window.setInterval(() => {
      this.data++;
    }, this.timer);
  }
}
