import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
})
export class TimerComponent implements OnInit {
  ngOnInit(): void {}

  timerSec: number = 1000;

  decrementTimer() {
    this.timerSec -= 10;
  }

  incrementTimer() {
    this.timerSec += 10;
  }
}
